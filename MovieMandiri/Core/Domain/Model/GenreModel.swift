//
//  GenreModel.swift
//  MovieMandiri
//
//  Created by agustiyan on 08/02/24.
//

import Foundation

struct GenreModel: Equatable, Identifiable {
  let id: Int
  let name: String
}
