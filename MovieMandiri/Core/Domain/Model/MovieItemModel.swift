//
//  MovieItemModel.swift
//  MovieMandiri
//
//  Created by agustiyan on 08/02/24.
//

import Foundation

struct MovieListModel: Equatable {
  let results: [MovieItemModel]
  let totalPages: Int
}

struct MovieItemModel: Equatable, Identifiable {
  let id: Int
  let title: String
  let posterPath: String
  
  var posterUrl: String {
    "https://image.tmdb.org/t/p/w500\(posterPath)"
  }
}
