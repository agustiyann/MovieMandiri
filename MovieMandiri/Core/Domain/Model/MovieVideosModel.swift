//
//  MovieVideosModel.swift
//  MovieMandiri
//
//  Created by agustiyan on 09/02/24.
//

import Foundation

struct MovieVideosModel: Equatable {
  let results: [MovieVideoModel]
}

struct MovieVideoModel: Equatable {
  let key: String
  let type: String
}
