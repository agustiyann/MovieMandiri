//
//  MovieReviewsModel.swift
//  MovieMandiri
//
//  Created by agustiyan on 09/02/24.
//

import Foundation

struct MovieReviewsModel: Equatable {
  let results: [MovieReviewModel]
  let totalPages: Int
}

struct MovieReviewModel: Equatable, Hashable {
  let author: String?
  let content: String?
  let createdAt: String?
  let authorDetails: AuthorDetailModel?
}

struct AuthorDetailModel: Equatable, Hashable {
  let avatarPath: String?
  
  var avatarUrl: String {
    "https://image.tmdb.org/t/p/w45\(avatarPath ?? "")"
  }
}
