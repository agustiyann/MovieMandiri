//
//  MovieDetailModel.swift
//  MovieMandiri
//
//  Created by agustiyan on 09/02/24.
//

import Foundation

struct MovieDetailModel: Equatable, Identifiable {
  let id: Int
  let title: String
  let posterPath: String
  let backdropPath: String
  let releaseDate: String
  let overview: String
  let status: String
  let genres: [GenreModel]
  
  var posterUrl: String {
    "https://image.tmdb.org/t/p/w500\(posterPath)"
  }
}
