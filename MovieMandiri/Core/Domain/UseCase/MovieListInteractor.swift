//
//  MovieListInteractor.swift
//  MovieMandiri
//
//  Created by agustiyan on 08/02/24.
//

import Foundation

protocol MovieListUseCase {
  func getMoviesByGenre(page: Int, completion: @escaping (Result<MovieListModel, BaseError>) -> Void)
  func getGenre() -> GenreModel
}

class MovieListInteractor: MovieListUseCase {
  private let repository: MovieRepositoryProtocol
  private let genre: GenreModel

  init(repository: MovieRepositoryProtocol, genre: GenreModel) {
    self.repository = repository
    self.genre = genre
  }
  
  func getMoviesByGenre(page: Int, completion: @escaping (Result<MovieListModel, BaseError>) -> Void) {
    repository.getMoviesByGenreId(id: genre.id, page: page) { result in
      completion(result)
    }
  }
  
  func getGenre() -> GenreModel {
    genre
  }
}
