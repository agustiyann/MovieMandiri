//
//  MovieDetailInteractor.swift
//  MovieMandiri
//
//  Created by agustiyan on 09/02/24.
//

import Foundation

protocol MovieDetailUseCase {
  func getId() -> Int
  func getMovieInfo(completion: @escaping (Result<MovieDetailModel, BaseError>) -> Void)
  func getMovieTrailer(completion: @escaping (Result<MovieVideoModel, BaseError>) -> Void)
  func getMovieReviews(page: Int, completion: @escaping (Result<MovieReviewsModel, BaseError>) -> Void)
}

class MovieDetailInteractor: MovieDetailUseCase {
  private let repository: MovieRepositoryProtocol
  private let id: Int

  init(repository: MovieRepositoryProtocol, id: Int) {
    self.repository = repository
    self.id = id
  }
  
  func getId() -> Int {
    id
  }
  
  func getMovieInfo(completion: @escaping (Result<MovieDetailModel, BaseError>) -> Void) {
    repository.getMovieInfo(id: id) { result in
      completion(result)
    }
  }
  
  func getMovieTrailer(completion: @escaping (Result<MovieVideoModel, BaseError>) -> Void) {
    repository.getMovieTrailer(id: id) { result in
      completion(result)
    }
  }
  
  func getMovieReviews(page: Int, completion: @escaping (Result<MovieReviewsModel, BaseError>) -> Void) {
    repository.getMovieReviews(id: id, page: page) { result in
      completion(result)
    }
  }
}
