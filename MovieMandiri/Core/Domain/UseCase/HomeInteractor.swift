//
//  HomeInteractor.swift
//  MovieMandiri
//
//  Created by agustiyan on 08/02/24.
//

import Foundation

protocol HomeUseCase {
  func getGenres(completion: @escaping (Result<[GenreModel], BaseError>) -> Void)
}

class HomeInteractor: HomeUseCase {
  private let repository: MovieRepositoryProtocol

  init(repository: MovieRepositoryProtocol) {
    self.repository = repository
  }

  func getGenres(completion: @escaping (Result<[GenreModel], BaseError>) -> Void) {
    repository.getGenres { result in
      completion(result)
    }
  }
}
