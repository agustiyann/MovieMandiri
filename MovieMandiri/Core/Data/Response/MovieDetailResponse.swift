//
//  MovieDetailResponse.swift
//  MovieMandiri
//
//  Created by agustiyan on 08/02/24.
//

import Foundation

struct MovieDetailResponse: Decodable {
  let id: Int
  let title: String?
  let posterPath: String?
  let backdropPath: String?
  let releaseDate: String?
  let overview: String?
  let status: String?
  let genres: [GenreResponse]?
}
