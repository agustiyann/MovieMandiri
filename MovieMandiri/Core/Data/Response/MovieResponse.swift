//
//  MovieResponse.swift
//  MovieMandiri
//
//  Created by agustiyan on 08/02/24.
//

import Foundation

struct MovieListResponse: Decodable {
  let results: [MovieItemResponse]
  let totalPages: Int
}

struct MovieItemResponse: Decodable {
  let id: Int
  let title: String?
  let posterPath: String?
}
