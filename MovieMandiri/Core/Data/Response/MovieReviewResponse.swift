//
//  MovieReviewResponse.swift
//  MovieMandiri
//
//  Created by agustiyan on 09/02/24.
//

import Foundation

struct MovieReviewsResponse: Decodable, Equatable {
  let results: [MovieReviewResponse]
  let totalPages: Int
}

struct MovieReviewResponse: Decodable, Equatable {
  let author: String?
  let content: String?
  let createdAt: String?
  let authorDetails: AuthorDetailResponse?
}

struct AuthorDetailResponse: Decodable, Equatable {
  let avatarPath: String?
}

