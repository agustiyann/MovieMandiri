//
//  GenreResponse.swift
//  MovieMandiri
//
//  Created by agustiyan on 08/02/24.
//

import Foundation

struct GenreListResponse: Codable {
  let genres: [GenreResponse]
}

struct GenreResponse: Codable {
  let id: Int?
  let name: String?
}
