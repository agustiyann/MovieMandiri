//
//  MovieVideosResponse.swift
//  MovieMandiri
//
//  Created by agustiyan on 09/02/24.
//

import Foundation

struct MovieVideosResponse: Decodable, Equatable {
  let results: [MovieVideoResponse]
}

struct MovieVideoResponse: Decodable, Equatable {
  let key: String?
  let type: String?
}
