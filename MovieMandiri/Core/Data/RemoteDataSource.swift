//
//  RemoteDataSource.swift
//  MovieMandiri
//
//  Created by agustiyan on 08/02/24.
//

import Foundation

protocol RemoteDataSourceProtocol: AnyObject {
  func getGenres(result: @escaping (Result<[GenreResponse], BaseError>) -> Void)
  func getMoviesByGenreId(id: Int, page: Int, result: @escaping (Result<MovieListResponse, BaseError>) -> Void)
  func getMovieInfo(id: Int, result: @escaping (Result<MovieDetailResponse, BaseError>) -> Void)
  func getMovieTrailer(id: Int, result: @escaping (Result<[MovieVideoResponse], BaseError>) -> Void)
  func getMovieReviews(id: Int, page: Int, result: @escaping (Result<MovieReviewsResponse, BaseError>) -> Void)
}

final class RemoteDataSource: NSObject {
  private let decoder: JSONDecoder = {
      let decoder = JSONDecoder()
      decoder.keyDecodingStrategy = .convertFromSnakeCase
      return decoder
  }()
  
  override init() {}

  static let sharedInstance: RemoteDataSource = RemoteDataSource()
}

extension RemoteDataSource: RemoteDataSourceProtocol {

  func getGenres(result: @escaping (Result<[GenreResponse], BaseError>) -> Void) {
    guard let url = MovieApiFactoryImpl.shared.createGenresUrl().url else {
      return
    }

    let task = URLSession.shared.dataTask(with: url) { data, response, error in
      if error != nil {
        result(.failure(.errorNetwork))
        return
      }

      guard let data else {
        result(.failure(.invalidData))
        return
      }

      do {
        let genres = try self.decoder.decode(GenreListResponse.self, from: data).genres
        result(.success(genres))
      } catch {
        result(.failure(.invalidResponse))
      }
    }
    task.resume()
  }

  func getMoviesByGenreId(id: Int, page: Int, result: @escaping (Result<MovieListResponse, BaseError>) -> Void) {
    guard let url = MovieApiFactoryImpl.shared.createMoviesByGenreIdUrl(id: id, page: page).url else {
      return
    }

    let task = URLSession.shared.dataTask(with: url) { data, response, error in
      if error != nil {
        result(.failure(.errorNetwork))
        return
      }

      guard let data else {
        result(.failure(.invalidData))
        return
      }

      do {
        let movie = try self.decoder.decode(MovieListResponse.self, from: data)
        result(.success(movie))
      } catch {
        result(.failure(.invalidResponse))
      }
    }
    task.resume()
  }
  
  func getMovieInfo(id: Int, result: @escaping (Result<MovieDetailResponse, BaseError>) -> Void) {
    guard let url = MovieApiFactoryImpl.shared.createMovieDetailUrl(id: id).url else {
      return
    }
    
    let task = URLSession.shared.dataTask(with: url) { data, response, error in
      if error != nil {
        result(.failure(.errorNetwork))
        return
      }

      guard let data else {
        result(.failure(.invalidData))
        return
      }

      do {
        let detail = try self.decoder.decode(MovieDetailResponse.self, from: data)
        result(.success(detail))
      } catch {
        result(.failure(.invalidResponse))
      }
    }
    task.resume()
  }
  
  func getMovieTrailer(id: Int, result: @escaping (Result<[MovieVideoResponse], BaseError>) -> Void) {
    guard let url = MovieApiFactoryImpl.shared.createMovieTrailerUrl(id: id).url else {
      return
    }
    
    let task = URLSession.shared.dataTask(with: url) { data, response, error in
      if error != nil {
        result(.failure(.errorNetwork))
        return
      }

      guard let data else {
        result(.failure(.invalidData))
        return
      }

      do {
        let trailer = try self.decoder.decode(MovieVideosResponse.self, from: data).results
        result(.success(trailer))
      } catch {
        result(.failure(.invalidResponse))
      }
    }
    task.resume()
  }
  
  func getMovieReviews(id: Int, page: Int, result: @escaping (Result<MovieReviewsResponse, BaseError>) -> Void) {
    guard let url = MovieApiFactoryImpl.shared.createMovieReviewsUrl(id: id, page: page).url else {
      return
    }
    
    let task = URLSession.shared.dataTask(with: url) { data, response, error in
      if error != nil {
        result(.failure(.errorNetwork))
        return
      }

      guard let data else {
        result(.failure(.invalidData))
        return
      }

      do {
        let trailer = try self.decoder.decode(MovieReviewsResponse.self, from: data)
        result(.success(trailer))
      } catch {
        result(.failure(.invalidResponse))
      }
    }
    task.resume()
  }

}
