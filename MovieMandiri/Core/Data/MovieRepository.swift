//
//  MovieRepository.swift
//  MovieMandiri
//
//  Created by agustiyan on 08/02/24.
//

import Foundation

protocol MovieRepositoryProtocol: AnyObject {
  func getGenres(completion: @escaping (Result<[GenreModel], BaseError>) -> Void)
  func getMoviesByGenreId(id: Int, page: Int, completion: @escaping (Result<MovieListModel, BaseError>) -> Void)
  func getMovieInfo(id: Int, completion: @escaping (Result<MovieDetailModel, BaseError>) -> Void)
  func getMovieTrailer(id: Int, completion: @escaping (Result<MovieVideoModel, BaseError>) -> Void)
  func getMovieReviews(id: Int, page: Int, completion: @escaping (Result<MovieReviewsModel, BaseError>) -> Void)
}

final class MovieRepository: NSObject {
  typealias MovieInstance = (RemoteDataSource) -> MovieRepository

  fileprivate let remote: RemoteDataSource

  private init(remote: RemoteDataSource) {
    self.remote = remote
  }

  static let sharedInstance: MovieInstance = { remoteRepo in
    return MovieRepository(remote: remoteRepo)
  }
}

extension MovieRepository: MovieRepositoryProtocol {

  // MARK: - Genres
  func getGenres(completion: @escaping (Result<[GenreModel], BaseError>) -> Void) {
    self.remote.getGenres { result in
      switch result {
      case .success(let genreResponse):
        let genreList = GenreMapper.mapGenresResponseToDomain(input: genreResponse)
        completion(.success(genreList))
      case .failure(let error):
        completion(.failure(error))
      }
    }
  }

  // MARK: - Movies
  func getMoviesByGenreId(id: Int, page: Int, completion: @escaping (Result<MovieListModel, BaseError>) -> Void) {
    self.remote.getMoviesByGenreId(id: id, page: page) { result in
      switch result {
      case .success(let moviesResponse):
        let movieList = MoviesMapper.mapMovieListResponseToDomain(input: moviesResponse)
        completion(.success(movieList))
      case .failure(let error):
        completion(.failure(error))
      }
    }
  }
  
  // MARK: - Movie Detail
  func getMovieInfo(id: Int, completion: @escaping (Result<MovieDetailModel, BaseError>) -> Void) {
    self.remote.getMovieInfo(id: id) { result in
      switch result {
      case .success(let detail):
        let detailModel = MoviesMapper.mapMovieDetailResponseToDomain(input: detail)
        completion(.success(detailModel))
      case .failure(let failure):
        completion(.failure(failure))
      }
    }
  }
  
  // MARK: - Trailer
  func getMovieTrailer(id: Int, completion: @escaping (Result<MovieVideoModel, BaseError>) -> Void) {
    self.remote.getMovieTrailer(id: id) { result in
      switch result {
      case .success(let videos):
        let videosModel = MoviesMapper.mapMovieVideosResponseToDomain(input: videos)
        let video = self.getTrailer(videos: videosModel)
        completion(.success(video))
      case .failure(let failure):
        completion(.failure(failure))
      }
    }
  }
  
  private func getTrailer(videos: [MovieVideoModel]) -> MovieVideoModel {
    for video in videos {
      if video.type == "Trailer" {
        return video
      }
    }
    if videos.count > 0 {
      return videos[0]
    }
    return MovieVideoModel(key: "", type: "")
  }
  
  // MARK: - Reviews
  func getMovieReviews(id: Int, page: Int, completion: @escaping (Result<MovieReviewsModel, BaseError>) -> Void) {
    self.remote.getMovieReviews(id: id, page: page) { result in
      switch result {
      case .success(let reviews):
        let reviewsModel = MoviesMapper.mapReviewsResponseToDomain(input: reviews)
        completion(.success(reviewsModel))
      case .failure(let failure):
        completion(.failure(failure))
      }
    }
  }

}
