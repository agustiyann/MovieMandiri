//
//  MovieApiFactory.swift
//  MovieMandiri
//
//  Created by agustiyan on 08/02/24.
//

import Foundation

protocol MovieApiFactory {
  func createGenresUrl() -> URLComponents
  func createMoviesByGenreIdUrl(id: Int, page: Int) -> URLComponents
  func createMovieDetailUrl(id: Int) -> URLComponents
  func createMovieReviewsUrl(id: Int, page: Int) -> URLComponents
  func createMovieTrailerUrl(id: Int) -> URLComponents
}

struct MovieApiFactoryImpl: MovieApiFactory {

  static let shared = MovieApiFactoryImpl()

  private let apiKey = "a6beac03fb8ef024b93e511757777e5c"

  private func createBaseUrl() -> URLComponents {
    let language = "en-US"
    var components = URLComponents()
    components.scheme = "https"
    components.host = "api.themoviedb.org"
    components.queryItems = [
      URLQueryItem(name: "api_key", value: apiKey),
      URLQueryItem(name: "language", value: language)
    ]
    return components
  }

  func createGenresUrl() -> URLComponents {
    var components = createBaseUrl()
    components.path = "/3/genre/movie/list"
    
    return components
  }
  
  func createMoviesByGenreIdUrl(id: Int, page: Int) -> URLComponents {
    var components = createBaseUrl()
    components.path = "/3/discover/movie"
    components.queryItems?.append(URLQueryItem(name: "with_genres", value: "\(id)"))
    components.queryItems?.append(URLQueryItem(name: "page", value: "\(page)"))
    
    return components
  }
  
  func createMovieDetailUrl(id: Int) -> URLComponents {
    var components = createBaseUrl()
    components.path = "/3/movie/\(id)"
    
    return components
  }
  
  func createMovieReviewsUrl(id: Int, page: Int) -> URLComponents {
    var components = createBaseUrl()
    components.path = "/3/movie/\(id)/reviews"
    components.queryItems?.append(URLQueryItem(name: "page", value: "\(page)"))
    
    return components
  }
  
  func createMovieTrailerUrl(id: Int) -> URLComponents {
    var components = createBaseUrl()
    components.path = "/3/movie/\(id)/videos"
    
    return components
  }
  
}
