//
//  YTWrapper.swift
//  MovieMandiri
//
//  Created by agustiyan on 09/02/24.
//

import Foundation
import SwiftUI
import YouTubeiOSPlayerHelper

struct YTWrapper : UIViewRepresentable {
  var videoID : String
  
  func makeUIView(context: Context) -> YTPlayerView {
    let playerView = YTPlayerView()
    return playerView
  }
  
  func updateUIView(_ uiView: YTPlayerView, context: Context) {
    uiView.load(withVideoId: videoID)
  }
}
