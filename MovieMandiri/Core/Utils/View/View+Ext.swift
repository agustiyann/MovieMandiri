//
//  View+Ext.swift
//  MovieMandiri
//
//  Created by agustiyan on 09/02/24.
//

import SwiftUI

extension View {
  func getRect() -> CGRect {
    return UIScreen.main.bounds
  }
}
