//
//  GenreMapper.swift
//  MovieMandiri
//
//  Created by agustiyan on 08/02/24.
//

import Foundation

final class GenreMapper {

  static func mapGenresResponseToDomain(input genresResponse: [GenreResponse]) -> [GenreModel] {
    return genresResponse.map { genre in
      GenreModel(id: genre.id ?? Int(),
                 name: genre.name ?? "Unknow")
    }
  }

}
