//
//  MoviesMapper.swift
//  MovieMandiri
//
//  Created by agustiyan on 08/02/24.
//

import Foundation

final class MoviesMapper {
  
  static func mapMovieListResponseToDomain(input moviesResponse: MovieListResponse) -> MovieListModel {
    return MovieListModel(results: mapMoviesResponseToDomain(input: moviesResponse.results),
                          totalPages: moviesResponse.totalPages)
  }

  static func mapMoviesResponseToDomain(input moviesRespose: [MovieItemResponse]) -> [MovieItemModel] {
    return moviesRespose.map { movie in
      return MovieItemModel(id: movie.id,
                            title: movie.title ?? "Unknow",
                            posterPath: movie.posterPath ?? "Unknow")
    }
  }
  
  static func mapMovieDetailResponseToDomain(input movieDetailResponse: MovieDetailResponse) -> MovieDetailModel {
    return MovieDetailModel(id: movieDetailResponse.id,
                            title: movieDetailResponse.title ?? "Unknow",
                            posterPath: movieDetailResponse.posterPath ?? "Unknow",
                            backdropPath: movieDetailResponse.backdropPath ?? "Unknow",
                            releaseDate: movieDetailResponse.releaseDate ?? "Unknow",
                            overview: movieDetailResponse.overview ?? "Unknow",
                            status: movieDetailResponse.status ?? "Unknow",
                            genres: GenreMapper.mapGenresResponseToDomain(input: movieDetailResponse.genres ?? []))
  }
  
  static func mapMovieVideosResponseToDomain(input movieVideosResponse: [MovieVideoResponse]) -> [MovieVideoModel] {
    return movieVideosResponse.map { video in
      return MovieVideoModel(key: video.key ?? "", type: video.type ?? "")
    }
  }
  
  static func mapReviewsResponseToDomain(input reviews: MovieReviewsResponse) -> MovieReviewsModel {
    return MovieReviewsModel(results: mapMovieReviewsResponseToDomain(input: reviews.results),
                             totalPages: reviews.totalPages)
  }
  
  static func mapMovieReviewsResponseToDomain(input moviewReviewsResponse: [MovieReviewResponse]) -> [MovieReviewModel] {
    return moviewReviewsResponse.map { review in
      return MovieReviewModel(author: review.author ?? "Unknow",
                              content: review.content ?? "Unknow",
                              createdAt: review.createdAt ?? "Unknow",
                              authorDetails: mapReviewAuthorResponseToDomain(input: review.authorDetails))
    }
  }
  
  static func mapReviewAuthorResponseToDomain(input reviewAuthorResponse: AuthorDetailResponse?) -> AuthorDetailModel {
    return AuthorDetailModel(avatarPath: reviewAuthorResponse?.avatarPath)
  }

}
