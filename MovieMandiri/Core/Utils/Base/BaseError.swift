//
//  BaseError.swift
//  MovieMandiri
//
//  Created by agustiyan on 08/02/24.
//

import Foundation

enum BaseError: String, Error {
    case errorNetwork = "Found error from network. Please try again later."
    case invalidResponse = "Invalid data receiced from the server"
    case invalidData = "The data received from the server was invalid"
}
