//
//  Injection.swift
//  MovieMandiri
//
//  Created by agustiyan on 08/02/24.
//

import Foundation

final class Injection: NSObject {

  private func provideRepository() -> MovieRepositoryProtocol {
    let remote: RemoteDataSource = RemoteDataSource.sharedInstance
    return MovieRepository.sharedInstance(remote)
  }

  func provideHome() -> HomeUseCase {
    let repository = provideRepository()
    return HomeInteractor(repository: repository)
  }
  
  func provideMovieList(genre: GenreModel) -> MovieListUseCase {
    let repository = provideRepository()
    return MovieListInteractor(repository: repository, genre: genre)
  }
  
  func provideMovieDetail(id: Int) -> MovieDetailUseCase {
    let repository = provideRepository()
    return MovieDetailInteractor(repository: repository, id: id)
  }

}
