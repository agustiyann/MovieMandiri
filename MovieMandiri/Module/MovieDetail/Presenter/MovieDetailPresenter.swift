//
//  MovieDetailPresenter.swift
//  MovieMandiri
//
//  Created by agustiyan on 09/02/24.
//

import Foundation
import SwiftUI

class MovieDetailPresenter: ObservableObject {
  private let movieUseCase: MovieDetailUseCase
  
  @Published var reviewPage: Int = 1
  @Published var totalPages: Int = 1
  @Published var movie: MovieDetailModel? = nil
  @Published var movieTrailer: MovieVideoModel? = nil
  @Published var movieReviews: [MovieReviewModel] = []
  @Published var title: String = ""
  @Published var errorMessage: String = ""
  @Published var loadingState: Bool = false
  @Published var loadingVideoState: Bool = false
  @Published var loadingReviewsState: Bool = false
  
  init(movieUseCase: MovieDetailUseCase) {
    self.movieUseCase = movieUseCase
  }
  
  func getDetailMovie() {
    loadingState = true
    movieUseCase.getMovieInfo { result in
      switch result {
      case .success(let movie):
        DispatchQueue.main.async {
          self.loadingState = false
          self.movie = movie
        }
      case .failure(let failure):
        DispatchQueue.main.async {
          self.loadingState = false
          self.errorMessage = failure.rawValue
        }
      }
    }
  }
  
  func getTrailer() {
    loadingVideoState = true
    movieUseCase.getMovieTrailer { result in
      switch result {
      case .success(let movieTrailer):
        DispatchQueue.main.async {
          self.loadingVideoState = false
          self.movieTrailer = movieTrailer
        }
      case .failure(let failure):
        DispatchQueue.main.async {
          self.loadingVideoState = false
          self.errorMessage = failure.rawValue
        }
      }
    }
  }
  
  func getReviews() {
    loadingReviewsState = true
    movieUseCase.getMovieReviews(page: 1) { result in
      switch result {
      case .success(let reviews):
        DispatchQueue.main.async {
          self.loadingReviewsState = false
          self.movieReviews = reviews.results
        }
      case .failure(let failure):
        DispatchQueue.main.async {
          self.loadingReviewsState = false
          self.errorMessage = failure.rawValue
        }
      }
    }
  }
  
  func hasReachEnd(of review: MovieReviewModel) -> Bool {
    movieReviews.last?.author == review.author
  }
  
  func getNextSetOfMovies() {
    do {
      reviewPage += 1
      loadingReviewsState = true
      movieUseCase.getMovieReviews(page: reviewPage) { result in
        switch result {
        case .success(let reviews):
          DispatchQueue.main.async {
            self.loadingReviewsState = false
            self.movieReviews = reviews.results
          }
        case .failure(let failure):
          DispatchQueue.main.async {
            self.loadingReviewsState = false
            self.errorMessage = failure.rawValue
          }
        }
      }
    }
  }
  
  func refreshPage() {
    errorMessage = ""
    getTrailer()
    getDetailMovie()
    getReviews()
  }
  
}
