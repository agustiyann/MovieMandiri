//
//  MovieDetailView.swift
//  MovieMandiri
//
//  Created by agustiyan on 09/02/24.
//

import SwiftUI
import SDWebImageSwiftUI

struct MovieDetailView: View {
  @ObservedObject var presenter: MovieDetailPresenter
  
  var body: some View {
    List {
      if presenter.loadingState {
        Text("Loading...")
        ActivityIndicator()
      } else if !presenter.errorMessage.isEmpty {
        Text("\(presenter.errorMessage)")
          .font(.title)
          .bold()
          .multilineTextAlignment(.center)
        
        Button("Refresh") {
          self.presenter.refreshPage()
        }
      } else {
        YTWrapper(videoID: presenter.movieTrailer?.key ?? "")
          .aspectRatio(16/9, contentMode: .fit)
        
        HStack(alignment: .top) {
          WebImage(url: URL(string: presenter.movie?.posterUrl ?? ""))
            .resizable()
            .indicator(.activity)
            .transition(.fade(duration: 0.5))
            .scaledToFit()
            .frame(width: 100, height: 150)
          
          VStack(alignment: .leading) {
            Text(presenter.movie?.title ?? "-")
              .font(.title2)
              .bold()
            
            Text("\(presenter.movie?.status ?? "") - \(presenter.movie?.releaseDate ?? "")")
            
            Text((presenter.movie?.genres.map { $0.name } ?? []).joined(separator: ", "))
          }
        }
        
        VStack(alignment: .leading) {
          Text("Overview")
            .font(.headline)
          
          Text(presenter.movie?.overview ?? "")
        }
        
        Text("Reviews (\(presenter.movieReviews.count))")
          .font(.headline)
        
        ForEach(presenter.movieReviews, id: \.self) { review in
          HStack(alignment: .top) {
            WebImage(url: URL(string: review.authorDetails?.avatarUrl ?? ""))
              .placeholder(Image(systemName: "person"))
              .resizable()
              .indicator(.activity)
              .transition(.fade(duration: 0.5))
              .frame(width: 20, height: 20)
              .cornerRadius(8)
              .scaledToFill()
            
            VStack(alignment: .leading) {
              Text(review.author ?? "")
                .font(.headline)
              
              Text(review.content ?? "")
            }
          }
          .task {
            if presenter.hasReachEnd(of: review),
               presenter.reviewPage < presenter.totalPages {
              self.presenter.getNextSetOfMovies()
            }
          }
        }
        
      }
    }
    .refreshable {
      self.presenter.getTrailer()
      self.presenter.getDetailMovie()
      self.presenter.getReviews()
    }
    .navigationTitle("Movie Info")
    .listStyle(.plain)
    .onAppear {
      self.presenter.getTrailer()
      self.presenter.getDetailMovie()
      self.presenter.getReviews()
    }
  }
}

#Preview {
  @ObservedObject var presenter = MovieDetailPresenter(movieUseCase: Injection().provideMovieDetail(id: 866398))
  return MovieDetailView(presenter: presenter)
}
