//
//  MovieListPresenter.swift
//  MovieMandiri
//
//  Created by agustiyan on 08/02/24.
//

import Foundation
import SwiftUI

class MovieListPresenter: ObservableObject {
  private let router = MovieListRouter()
  private let movieListUseCase: MovieListUseCase

  @Published var page: Int = 1
  @Published var totalPages: Int = 1
  @Published var movies: [MovieItemModel] = []
  @Published var title: String = ""
  @Published var errorMessage: String = ""
  @Published var loadingState: Bool = false
  @Published var loadingNextState: Bool = false

  init(movieListUseCase: MovieListUseCase) {
    self.movieListUseCase = movieListUseCase
    self.title = movieListUseCase.getGenre().name
  }

  func getMoviesByGenreId() {
    loadingState = true
    movieListUseCase.getMoviesByGenre(page: 1) { result in
      switch result {
      case .success(let movies):
        DispatchQueue.main.async {
          self.loadingState = false
          self.totalPages = movies.totalPages
          self.movies = movies.results
        }
      case .failure(let error):
        DispatchQueue.main.async {
          self.loadingState = false
          self.errorMessage = error.rawValue
        }
      }
    }
  }
  
  func hasReachEnd(of movie: MovieItemModel) -> Bool {
    movies.last?.id == movie.id
  }
  
  func getNextSetOfMovies() {
    do {
      page += 1
      loadingNextState = true
      movieListUseCase.getMoviesByGenre(page: page) { result in
        switch result {
        case .success(let movies):
          DispatchQueue.main.async {
            self.loadingNextState = false
            self.movies += movies.results
          }
        case .failure(let error):
          DispatchQueue.main.async {
            self.loadingNextState = false
            self.errorMessage = error.rawValue
          }
        }
      }
    }
  }
  
  func refreshPage() {
    errorMessage = ""
    getMoviesByGenreId()
  }
  
  func linkBuilder<Content: View>(
    for id: Int,
    @ViewBuilder content: () -> Content
  ) -> some View {
    NavigationLink(destination: router.makeMovieDetailView(for: id)) {
      content()
    }
  }

}
