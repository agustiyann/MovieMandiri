//
//  MovieListView.swift
//  MovieMandiri
//
//  Created by agustiyan on 08/02/24.
//

import SwiftUI
import SDWebImageSwiftUI

struct MovieListView: View {
  @ObservedObject var presenter: MovieListPresenter
  
  let column: [GridItem] = Array(repeating: .init(.flexible()), count: 3)
  
  var body: some View {
    VStack {
      if presenter.loadingState {
        Text("Loading...")
        ActivityIndicator()
      } else if !presenter.errorMessage.isEmpty {
        Text("\(presenter.errorMessage)")
          .font(.title)
          .bold()
          .multilineTextAlignment(.center)
        
        Button("Refresh") {
          self.presenter.refreshPage()
        }
      } else {
        ScrollView {
          LazyVGrid(columns: column, content: {
            ForEach(presenter.movies, id: \.id) { movie in
              self.presenter.linkBuilder(for: movie.id) {
                VStack {
                  WebImage(url: URL(string: movie.posterUrl))
                    .resizable()
                    .indicator(.activity)
                    .transition(.fade(duration: 0.5))
                    .scaledToFit()
                    .frame(width: 100, height: 150)
                  Text(movie.title)
                    .frame(maxHeight: .infinity, alignment: .top)
                    .multilineTextAlignment(.center)
                    .lineLimit(2)
                }
                .task {
                  if presenter.hasReachEnd(of: movie),
                     presenter.page < presenter.totalPages {
                    self.presenter.getNextSetOfMovies()
                  }
                }
              }
            }
          })
        }
        .overlay(alignment: .bottom) {
          if presenter.loadingNextState {
            VStack {
              Text("Loading...")
              ActivityIndicator()
            }
          }
        }
      }
    }
    .refreshable {
      self.presenter.getMoviesByGenreId()
    }
    .onAppear {
      if self.presenter.movies.count == 0 {
        self.presenter.getMoviesByGenreId()
      }
    }
    .navigationTitle(self.presenter.title)
  }
}

#Preview {
  @ObservedObject var presenter = MovieListPresenter(movieListUseCase: Injection().provideMovieList(genre: GenreModel(id: 28, name: "Action")))
  return MovieListView(presenter: presenter)
}
