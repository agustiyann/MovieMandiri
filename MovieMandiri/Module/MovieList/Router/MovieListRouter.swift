//
//  MovieListRouter.swift
//  MovieMandiri
//
//  Created by agustiyan on 09/02/24.
//

import Foundation

import SwiftUI

class MovieListRouter {

  func makeMovieDetailView(for id: Int) -> some View {
    let movieUseCase = Injection.init().provideMovieDetail(id: id)
    let presenter = MovieDetailPresenter(movieUseCase: movieUseCase)
    return MovieDetailView(presenter: presenter)
  }

}
