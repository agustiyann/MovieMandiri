//
//  HomeRouter.swift
//  MovieMandiri
//
//  Created by agustiyan on 08/02/24.
//

import Foundation
import SwiftUI

class HomeRouter {

  func makeMovieListView(for genre: GenreModel) -> some View {
    let movieUseCase = Injection.init().provideMovieList(genre: genre)
    let presenter = MovieListPresenter(movieListUseCase: movieUseCase)
    return MovieListView(presenter: presenter)
  }

}
