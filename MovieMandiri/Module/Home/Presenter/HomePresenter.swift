//
//  HomePresenter.swift
//  MovieMandiri
//
//  Created by agustiyan on 08/02/24.
//

import Foundation
import SwiftUI

class HomePresenter: ObservableObject {
  private let router = HomeRouter()
  private let homeUseCase: HomeUseCase

  @Published var genres: [GenreModel] = []
  @Published var errorMessage: String = ""
  @Published var loadingState: Bool = false

  init(homeUseCase: HomeUseCase) {
    self.homeUseCase = homeUseCase
  }

  func getCategories() {
    loadingState = true
    homeUseCase.getGenres { result in
      switch result {
      case .success(let categories):
        DispatchQueue.main.async {
          self.loadingState = false
          self.genres = categories
        }
      case .failure(let error):
        DispatchQueue.main.async {
          self.loadingState = false
          self.errorMessage = error.rawValue
        }
      }
    }
  }
  
  func refreshPage() {
    errorMessage = ""
    getCategories()
  }

  func linkBuilder<Content: View>(
    for genre: GenreModel,
    @ViewBuilder content: () -> Content
  ) -> some View {
    NavigationLink(destination: router.makeMovieListView(for: genre)) {
      content()
    }
  }

}
