//
//  HomeView.swift
//  MovieMandiri
//
//  Created by agustiyan on 08/02/24.
//

import SwiftUI

struct HomeView: View {

  @ObservedObject var presenter: HomePresenter

  var body: some View {
    VStack {
      if presenter.loadingState {
        Text("Loading...")
        ActivityIndicator()
      } else if !presenter.errorMessage.isEmpty {
        Text("\(presenter.errorMessage)")
          .font(.title)
          .bold()
          .multilineTextAlignment(.center)
        
        Button("Refresh") {
          self.presenter.refreshPage()
        }
      } else {
        List {
          ForEach(presenter.genres, id: \.id) { genre in
            self.presenter.linkBuilder(for: genre) {
              Text(genre.name)
            }
          }
        }
      }
    }
    .refreshable {
      self.presenter.getCategories()
    }
    .onAppear {
      if self.presenter.genres.count == 0 {
        self.presenter.getCategories()
      }
    }
    .navigationTitle("Movie Genres")
  }
}

#Preview {
  @ObservedObject var presenter = HomePresenter(homeUseCase: Injection().provideHome())
  return HomeView(presenter: presenter)
}
