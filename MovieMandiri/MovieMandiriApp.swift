//
//  MovieMandiriApp.swift
//  MovieMandiri
//
//  Created by agustiyan on 08/02/24.
//

import SwiftUI

@main
struct MovieMandiriApp: App {
  let homePresenter = HomePresenter(homeUseCase: Injection.init().provideHome())

  var body: some Scene {
    WindowGroup {
      ContentView()
        .environmentObject(homePresenter)
    }
  }
}
