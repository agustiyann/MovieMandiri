//
//  ContentView.swift
//  MovieMandiri
//
//  Created by agustiyan on 08/02/24.
//

import SwiftUI

struct ContentView: View {
  @EnvironmentObject var homePresenter: HomePresenter

  var body: some View {
    NavigationView(content: {
      HomeView(presenter: homePresenter)
    })
  }
}

#Preview {
  let homePresenter = HomePresenter(homeUseCase: Injection.init().provideHome())
  return ContentView()
    .environmentObject(homePresenter)
}
