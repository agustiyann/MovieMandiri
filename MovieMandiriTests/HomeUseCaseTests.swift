//
//  HomeUseCaseTests.swift
//  MovieMandiriTests
//
//  Created by agustiyan on 08/02/24.
//

import XCTest

@testable import MovieMandiri
final class HomeUseCaseTests: XCTestCase {

  func test_positive_getGenreFromUseCase() {
    let useCase = MockPositiveGenreList()
    let resultExpectation = MockGenreList.generateGenreList()
    
    useCase.getGenres { result in
      switch result {
      case .success(let genres):
        XCTAssertEqual(genres, resultExpectation)
      default:
        break
      }
    }
  }

  func test_negative_getGenreFromUseCase() {
    let useCase = MockNegativeGenreList()
    let resultExpectation = BaseError.invalidResponse
    
    useCase.getGenres { result in
      switch result {
      case .failure(let error):
        XCTAssertEqual(error, resultExpectation)
      default:
        break
      }
    }
  }
  
}

struct MockGenreList {
  static func generateGenreList() -> [GenreModel] {
    return [GenreModel(id: 1, name: "Comedy")]
  }
}

struct MockPositiveGenreList: HomeUseCase {
  func getGenres(completion: @escaping (Result<[GenreModel], BaseError>) -> Void) {
    let result = MockGenreList.generateGenreList()
    completion(.success(result))
  }
}

struct MockNegativeGenreList: HomeUseCase {
  func getGenres(completion: @escaping (Result<[GenreModel], BaseError>) -> Void) {
    let result = BaseError.invalidResponse
    completion(.failure(result))
  }
}

