# MovieMandiri

This project is built in **Xcode version: 15.0.1**
Minimum Deployments: **iOS 16.0**

### Attachments:

|Video|Link  |
|--|--|
| Negative cases | https://drive.google.com/file/d/1aBAAmTWv4j5Gnh5eBzKSMZLLptVbqmpr/view?usp=sharing |
| Home (Genres) | https://drive.google.com/file/d/1-vP_IbUunN5TiSrhhkTG9XQ_mvFbGGJx/view?usp=sharing |
| Movies by genre | https://drive.google.com/file/d/1efPpfrUYKv8-DhohrmswBMA8qOUYImGJ/view?usp=sharing |
| Movie detail | https://drive.google.com/file/d/1YKkKbRCNuM2lPGlbdtknjxL4VsUVUGLk/view?usp=sharing |
